export const rollItemWithEffect = async (token, itemName, itemType, conditionName) => {
    if (!token) {
        ui.notifications.warn('Select an actor first');
        return;
    }

    if (!game.cub.conditions.find(c => c.name === conditionName)) {
        ui.notifications.warn(`No condition named "${conditionName}"`);
        return;
    }

    if (token.actor.data.effects.find(e => e.label === conditionName)) {
        game.cub.removeCondition(conditionName);
        return;
    }

    const item = token.actor.items.find(f => f.name === itemName && f.type === itemType);
    if (!item) {
        ui.notifications.warn(`${token.actor.name} doesn't have a ${itemType} named "${itemName}"`);
        return;
    }

    await item.roll();
    game.cub.addCondition(conditionName);
};
